//
//  ViewController.swift
//  joycard
//
//  Created by Ihwan ID on 28/03/20.
//  Copyright © 2020 Ihwan ID. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    var player: AVAudioPlayer?
    
    @IBOutlet weak var imageContainer: UIImageView!
    
    @IBOutlet weak var buttonContainer: UIButton!
    @IBOutlet weak var cardContainer: UIView!
    
    var joyColors = [UIColor.systemRed,UIColor.systemTeal, UIColor.systemPink, UIColor.systemBlue, UIColor.systemIndigo]
    
    var joySound = ["akad","spongebob"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cardContainer.layer.cornerRadius = 30
        buttonContainer.layer.cornerRadius = 15
    }

    @IBAction func buttonRandomizeTapped(_ sender: Any) {
        joyColors.shuffle()
        
        view.backgroundColor = joyColors[0]
        playSound()
    }
    
    func playSound() {
        joySound.shuffle()
        guard let url = Bundle.main.url(forResource: joySound[0], withExtension: "m4a") else { return }

        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)

            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)

            /* iOS 10 and earlier require the following line:
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */

            guard let player = player else { return }

            player.play()

        } catch let error {
            print(error.localizedDescription)
        }
    }
}

